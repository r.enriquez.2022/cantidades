'''
Maneja un diccionario con cantidades
'''
import sys
items = {}

def op_add():

    item = sys.argv[0]
    quantity = int(sys.argv[1])
    adlist = {item:quantity}
    items.update(adlist)
    return items

def op_items():

    for i in items.keys():
        print(i, end=" ")

def op_all():

    pass
    lista_item = []
    lista_cantidad = []
    if len(items) != 0:
        for item, quantitie in items.items():
            msg=f"{item} ({quantitie})"
            lista_item.append(msg)
        item_list = " ".join(lista_item)
        print(f"All: {item_list}")
    else:
        print("No hay items")

def op_sum():

    pass
    quantities = items.values()
    sum = 0
    for quantities in quantities:
        sum = sum + quantities

    print(f"Sum: {sum}")

def main():

    while sys.argv != 0:
        op = sys.argv.pop(0)
        if op == "add":
            op_add()
        elif op == "items":
            op_items()
        elif op == "sum":
            op_sum()
        elif op == "all":
            op_all()
    return

if __name__ == '__main__':
    main()
